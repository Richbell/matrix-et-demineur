package com.example.myapplication2


fun main(args: Array<String>) {

    open class Matrice <T> (line: Int, col: Int)  {
        private var rows : Int = 2
        private var columns : Int = 3
        private val grid:List<MutableList<T?>>

        init {
            this.rows = rows
            this.columns = columns
            this.grid =  List(rows){
                MutableList(columns){
                    null
                }
            }
        }



        val Matrix1 = arrayOf(intArrayOf(2, 3, 4), intArrayOf(5, 2, 3))
        val Matrix2 = arrayOf(intArrayOf(-4, 5, 3), intArrayOf(5, 6, 3))


        override fun toString(): String {
            var printingMatrice = ""
            for (i in 0 .. rows - 1) {
                printingMatrice = printingMatrice.plus("[")
                for (j in 0..columns - 1) {
                    if(grid[i][j] == null) {
                        printingMatrice = printingMatrice.plus("-")
                    } else {
                        printingMatrice = printingMatrice.plus(grid[i][j])
                    }
                    if(j < columns - 1) {
                        printingMatrice = printingMatrice.plus(",")
                    }
                }
                printingMatrice = printingMatrice.plus("]\n")
            }

            return "Matrice(grid= \n${printingMatrice})"
        }

        inner class SquareMatrix <T> (size: Int): Matrice <T>(size, size) {

        }

        operator fun set(i: Int, j: Int, value: T) {
            val multiplication = Array(rows) { IntArray(columns) }
            for (i in 0..rows - 1) {
                for (j in 0..columns - 1) {
                    multiplication[i][j] = Matrix1[i][j] + Matrix2[i][j]
                }
            }


        }

        fun isSquare():Boolean {
            return this.columns == this.rows

        }
    }


}